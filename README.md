Steven Sklar's Resume Generator
-------------------------------

### Instructions

By default, the program will collect resources in the `src` dir, process them where necessary,
and output the resume HTML and styles in the `dist` dir.

To run the development server, which watches the `src` and `src/public` dirs for changes and 
serves the contents of the `dist` dir on port 3000, run the following command.
```
go run main.go serve
```