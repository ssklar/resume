module bitbucket.org/ssklar/resume

go 1.13

require (
	github.com/fsnotify/fsnotify v1.4.7
	golang.org/x/sys v0.0.0-20191110163157-d32e6e3b99c4
	gopkg.in/yaml.v2 v2.2.5
)
