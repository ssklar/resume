package main

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"

	"github.com/fsnotify/fsnotify"
	yaml "gopkg.in/yaml.v2"
)

const srcDir, distDir = "src/", "dist/"

type Resume struct {
	Personal     PersonalInfo
	Education    Education
	Social       Social
	Technologies []map[string][]string
	Experience   []Experience
}

type PersonalInfo struct {
	Name    string
	Address string
	Phone   string
	Email   string
}

type Education struct {
	School  string
	ClassOf int `yaml:"classOf"`
	// Todo: Make dateStart and dateEnd time.Time objects with custom unmarshalling
	DateStart string `yaml:"dateStart"`
	DateEnd   string `yaml:"dateEnd"`
	Degree    string
	Gpa       float32
}

type Social struct {
	Github   string
	Linkedin string
}

type Experience struct {
	Employer string
	Title    string
	// Todo: Make dateStart and dateEnd time.Time objects with custom unmarshalling
	DateStart    string `yaml:"dateStart"`
	DateEnd      string `yaml:"dateEnd"`
	Achievements []string
}

type CompileResult struct {
	Success bool
	Error   error
}

func renderHTMLTemplate(w io.Writer, dataPath string, templatePath string) error {
	data, _ := ioutil.ReadFile(dataPath)
	var resume Resume
	err := yaml.Unmarshal(data, &resume)

	if err != nil {
		return err
	}

	t, err := template.ParseFiles(templatePath)
	if err != nil {
		return err
	}

	err = t.Execute(w, resume)

	return err
}

func copyFile(src string, dest string) error {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return err
	}

	sourceData, err := os.Open(src)
	if err != nil {
		return err
	}
	defer sourceData.Close()

	destData, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer destData.Close()

	written, err := io.Copy(destData, sourceData)
	log.Printf("Copied %v bytes: %v to %v", written, src, dest)
	return err

}

func sourceWatcher(srcDir string, compileResults chan<- CompileResult) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				log.Println("event:", event)
				if event.Op&fsnotify.Write == fsnotify.Write {
					log.Println("modified file:", event.Name)
					err := buildSite(srcDir, distDir)
					result := CompileResult{(err != nil), err}
					compileResults <- result
				}

			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	err = watcher.Add(srcDir)
	if err != nil {
		log.Fatal(err)
	}

	err = watcher.Add(path.Join(srcDir, "public"))
	if err != nil {
		log.Fatal(err)
	}
	<-done
}

func buildSite(srcDir string, distDir string) error {

	os.MkdirAll(distDir, os.ModePerm)

	// Copy all static files
	files, err := ioutil.ReadDir(path.Join(srcDir, "public"))
	if err != nil {
		return err
	}

	for _, f := range files {
		src := path.Join(srcDir, "public", f.Name())
		dest := path.Join(distDir, f.Name())
		err := copyFile(src, dest)
		if err != nil {
			return err
		}
	}

	// Render the template using the data stored in yml
	templatePath := path.Join(srcDir, "index.html")
	dataPath := path.Join(srcDir, "data.yml")
	outPath := path.Join(distDir, "index.html")

	writer, err := os.Create(outPath)
	if err != nil {
		return err
	}
	defer writer.Close()
	err = renderHTMLTemplate(writer, dataPath, templatePath)
	return err
}

func serve(dir string) {
	fs := http.FileServer(http.Dir(dir))
	http.Handle("/", fs)
	log.Println("listening on :3000...")
	if err := http.ListenAndServe(":3000", nil); err != http.ErrServerClosed {
		log.Fatal(err)
	}

}

func main() {

	// Parse command line args
	// Currently, there is only 1 accepted subcommand, which locally serves
	// the application and watches for changes.
	var serveCmd bool
	switch len(os.Args) {
	case 2:
		switch os.Args[1] {

		case "serve":
			serveCmd = true
		default:
			fmt.Println("Valid subcommand is 'serve")
		}
	}

	// Build the site
	err := buildSite(srcDir, distDir)
	if err != nil {
		log.Fatal(err)
	}

	if serveCmd {
		compileResults := make(chan CompileResult)

		go serve(distDir)
		go sourceWatcher(srcDir, compileResults)

		for r := range compileResults {
			if r.Error != nil {
				log.Println(r.Error)
			}
		}
	}
}
